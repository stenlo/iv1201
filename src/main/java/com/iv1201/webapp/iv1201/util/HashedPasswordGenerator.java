package com.iv1201.webapp.iv1201.util;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import java.text.MessageFormat;

/**
 * the HashedPasswordGenerator is used for creating 
 * hashed versions of the of passwords
 * @author User
 */
public class HashedPasswordGenerator {

    /**
     * creats a hashed version of the password
     * @param password 
     */
    
    public  String generateHash(String password) {
        // This is one way of generating a SHA-256 hash. I uses classes/methods
        // from the Google Guava project. See the Maven pom.xml file which
        // I've modified to include the Guava libraries. See the imports
        // above which show what is being used.
        String hash
                = Hashing.sha256()
                .hashString(password, Charsets.UTF_8).toString();

        String output = MessageFormat.format("{0} hashed to: {1}", password, hash);

        System.out.println(output);
        return hash;
    }
    /**
     * used for generating the hashed passwords
     * @param args 
     */
/*
    public static void main(String[] args) {
        
        generateHash("w19nk23a");
        generateHash("password2");
        generateHash("password3");
    }
    */
}