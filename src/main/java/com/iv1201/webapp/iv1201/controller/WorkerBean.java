/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iv1201.webapp.iv1201.controller;

import com.iv1201.webapp.iv1201.entity.Groups;
import com.iv1201.webapp.iv1201.entity.Person;
import com.iv1201.webapp.iv1201.entity.Users;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author User
 */
@TransactionAttribute(TransactionAttributeType.MANDATORY)
@Stateless
public class WorkerBean 
{
    @PersistenceContext(unitName = "myPU")
    private EntityManager em;
    public void addUser(String username, String password) 
    {
        Users en = new Users(username,password);
        em.persist(en);
    }
    public void addPerson(String username, String password, String firstName, String lastName, String snn, String mail)
    {
        
        Person person = new Person (username, password, firstName, lastName, snn, mail);
        em.persist(person);
       
    }
    public void addUserToGroup(String username)
    {
        Groups gr = new Groups(username,"user");
        em.persist(gr);
    }
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}