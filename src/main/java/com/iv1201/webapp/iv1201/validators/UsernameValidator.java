/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iv1201.webapp.iv1201.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author mexak
 */
@FacesValidator("usernameValidator")
public class UsernameValidator implements Validator {

    private final String numberRegex="^\\s*$/"; //siffror ger error
    /**
     * Validation of the username is being made here 
     * If the username contains integers OR is shorter than 10 characters an error message will be printed
     * @param context
     * @param component
     * @param value
     * @throws ValidatorException 
     */
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String param = value.toString();
        
        if(param.matches(numberRegex)|| param.length()<10){
            FacesMessage msg = new FacesMessage("Invalid username");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        throw new ValidatorException(msg); 
    }
    
   }
}
