/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iv1201.webapp.iv1201.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author mexak
 */
@FacesValidator("passwordValidator")
public class PasswordValidator implements Validator {

    
    /**
     * In this method a validation of the Password is being made 
     * The password cannot be shorter than 4 integers 
     * If the password is shorter than 4 integers an error message will be printed
     * @param context
     * @param component
     * @param value
     * @throws ValidatorException 
     */
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String param = value.toString();
        
        if(param.length()<4){
            FacesMessage msg = new FacesMessage("Password is too short");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        throw new ValidatorException(msg); 
    }
    
   }
}
