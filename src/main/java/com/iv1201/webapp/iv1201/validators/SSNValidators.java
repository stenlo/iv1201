/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iv1201.webapp.iv1201.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author User
 */

@FacesValidator("ssnValidator")
public class SSNValidators implements Validator{
     private final String numberRegex ="^[0-9]$";
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException 
    {
        String param = value.toString();
        
        if(param.length()>12 || param.matches(numberRegex)|| param.length()<12)
        {
            if((param.length()<12))
            {
                FacesMessage msg = new FacesMessage("The SSN seems to be to short. Please try again");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
            if((param.length()>12))
            {
                FacesMessage msg = new FacesMessage("The SSN seems to be to long. Please try again");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
            if(param.matches(numberRegex))
            {
                FacesMessage msg = new FacesMessage("The SSN Should only contain numerals. Please try again");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
         
        }
    
   }
}
