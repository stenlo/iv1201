package com.iv1201.webapp.iv1201.jsfbean;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.*;

/**
 * This JSF/CDI Managed Bean provides a way for users to log out of the
 * application.
 */

/**
 * used to log out from the webpage
 * @author User
 */
@Named(value = "logoutBean")
@RequestScoped
public class LogoutBean 
{
   
    //log
    private static Logger log = Logger.getLogger(LogoutBean.class.getName());

    /**
     * used to invalidate and logout the user
     * @return 
     */
    public String logout() 
    {
       
        //page that you're gonna get redirected to
        String destination = "/index?faces-redirect=true";

        // FacesContext provides access to other container managed objects,
        // such as the HttpServletRequest object, which is needed to perform
        // the logout operation.
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = 
                (HttpServletRequest) context.getExternalContext().getRequest();

       // try 
        //{
            HttpSession session = request.getSession();
            //invalidate the session
            session.invalidate();
            
            //null out the user Principle
            //request.logout();
       /* } catch (Exception e) 
        {
            log.log(Level.SEVERE, "Failed to logout user!", e);
            destination = "/loginerror?faces-redirect=true";
        }*/

        return destination; // go to destination
    }
}
