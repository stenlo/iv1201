/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iv1201.webapp.iv1201.jsfbean;

import com.iv1201.webapp.iv1201.controller.*;
import com.iv1201.webapp.iv1201.util.HashedPasswordGenerator;
import javax.inject.Named;
import java.util.*;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityExistsException;

/**
 * used to print "worked" to the console. are going to contain the register methods 
 * in later stages of the development
 * @author User
 */
@Named(value = "registerBean")
@RequestScoped

/**
 * handles registration of a user
 */
public class RegisterBean 
{
    @EJB
    ControllerBean bean;
    private String firstName="";
    private String lastName="";
    private String ssn="";
    private String mail="";
    private String username="";
    private String password="";
    
    /**
     * used to redirect the user to the register.xhtml
     * 
     * @return returns a redirect command to to the facet
     */
    public String toReg()
    {
        return "/register?faces-redirect=true";
    }
    
    /**
     * register a users username hashed password in the users table and registers the same username in
     * the groups table
     * 
     * @return 
     */
    public String reg()
    {
        String destination = "/registrationSucces?faces-redirect=true";
        //hash the password for the database
        HashedPasswordGenerator hpg = new HashedPasswordGenerator();
        String hashedpassword =  hpg.generateHash(password);
       
            bean.regUser(username, hashedpassword, firstName, lastName, ssn, mail);
        
       
            destination = "/registrationFailed?faces-redirect=true";
        
        //bean.regGroup(username);
        System.out.println(username);
        System.out.println(hashedpassword);
        return destination;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstName;
    }

    public void setFirstname(String firstname) {
        this.firstName = firstname;
    }

    public String getLastname() {
        return lastName;
    }

    public void setLastname(String lastname) {
        this.lastName = lastname;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    
}